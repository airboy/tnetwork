# -*- coding: utf-8 -*-

from .models import Activity
from api.daymatch.models import Daymatch
from api.discus.models import (Discus, DiscusSon)
from api.entry.models import Entry
from datetime import date, timedelta

def get_public_activities(current_user):
	"""
	Return a list of HTML String contains the description about all public activity :

	OBJECTS | TASKS         | STATUS
	---------------------------------
	MATCH   | UPDATE        | SUCCESS
	DISCUS  | NEW           | SUCCESS
	SONS    | NEW           | SUCCESS
	ENTRY   | PUBLISH, LIKE | SUCCESS
	"""
	objects = ['MATCH', 'DISCUS', 'SONS', 'ENTRY']
	tasks = ['UPDATE', 'NEW', 'PUBLISH', 'LIKE']
	status = ['SUCCESS']

	# Get target date (last two weeks)
	today = date.today()
	target_date = today - timedelta(days=14)

	# Get all public get_public_activities
	activities = Activity.objects.filter(f_object__in=objects, f_task__in=tasks, f_status__in=status, date__gte=target_date).order_by('-date')

	# Format activity into HTML string
	result = []

	for activity in activities:
		# Get HTML for current activity
		html = get_html_activity(activity, current_user)

		# Append current HTML string to the list of activities
		result.append((html, activity.date))

	return result

def get_html_activity(activity, current_user):
	"""
	Return a HTML string containing the notification body.
	"""
	html = u'<span>'

	# Get owner of this activity
	if activity.f_user == current_user:
		html += u'<strong>Vous</strong> avez '
	else:
		html += u'<strong>' + activity.f_user.get_middle_name() + u'</strong> a '

	# Specifiy the action which has been done
	if activity.f_object == 'MATCH':
		# Get daymatch instance
		try:
			daymatch = Daymatch.objects.get(pk=activity.target_id)
		except Daymatch.DoesNotExist:
			# Add log
			pass

		if activity.f_task == 'UPDATE':
			dmdate = daymatch.date.strftime("%Y/%m/%d")
			html += u'mis à jour le jour de match du '
			html += u'<a href="/app/daymatch/' + dmdate + u'/">' + dmdate + u'</a>.'

	elif activity.f_object == 'DISCUS':
		# Get discus instance and daymatch date
		try:
			discus = Discus.objects.get(pk=activity.target_id)
			daymatch = Daymatch.objects.get(pk=discus.f_daymatch.id)
		except DoesNotExist:
			# Add log
			pass

		if activity.f_task == 'NEW':
			dmdate = daymatch.date.strftime("%Y/%m/%d")
			html += u'commenté le match du ' + dmdate + u' : "<i>'
			html +=  discus.body + u'</i>".'

	elif activity.f_object == 'SONS':
		# Get sons, discus instances and daymatch date
		try:
			son = DiscusSon.objects.get(pk=activity.target_id)
			discus = Discus.objects.get(pk=son.f_discus.id)
			daymatch = Daymatch.objects.get(pk=discus.f_daymatch.id)
		except DoesNotExist:
			# Add log
			pass

		if activity.f_task == 'NEW':
			dmdate = daymatch.date.strftime("%Y/%m/%d")
			html += u'répondu au commentaire de <strong>' + discus.author.get_middle_name() + u'</strong> '
			html += u'pour le match du ' + dmdate + u' : "<i>'
			html +=  son.body + u'</i>".'

	elif activity.f_object == 'ENTRY':
		# Get entry instances
		try:
			entry = Entry.objects.get(pk=activity.target_id)
		except Entry.DoesNotExist:
			# Add log
			pass

		if activity.f_task == 'PUBLISH':
			html += u'publié un nouveau post : '
			html +=  u'"<i>' + entry.title + u'</i>".'
		elif activity.f_task == 'LIKE':
			html += u'aimé le post : '
			html +=  u'"<i>' + entry.title + u'</i>".'

	html += u'</span>'

	return html