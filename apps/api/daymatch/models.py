# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.utils import timezone

# --- Schedule ----------
class Schedule(models.Model):
    description = models.CharField(max_length=5)

    def __unicode__(self):
        return self.description

# --- Playgroup ----------
class Playgroup(models.Model):
    name = models.CharField(max_length=20)
    is_active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

# --- Daymatch ------------
class Daymatch(models.Model):
    date = models.DateField()
    week = models.SmallIntegerField(null=True, blank=True)
    validated = models.BooleanField(default=0, verbose_name="Validation")

    class Meta:
        app_label = 'daymatch'
        ordering = ['-date']

    def save_first(self):
        self.week = self.date.isocalendar()[1]
        super(Daymatch, self).save()

    def save(self):
        super(Daymatch, self).save()

    def __unicode__(self):
        return "%s/%s/%s" % (self.date.day, self.date.month, self.date.year)

    def validate(self):
        self.validated = True
        self.save(update_fields=['validated'])
        return True

# --- DaymatchProg ------------
class DaymatchProg(models.Model):
    f_daymatch = models.ForeignKey(Daymatch)
    f_schedule = models.ForeignKey(Schedule)
    f_player = models.ForeignKey(settings.AUTH_USER_MODEL)

    def save(self):
        super(DaymatchProg, self).save()

    def __unicode__(self):
        return "%s (%s) : %s" % (self.f_daymatch, self.f_schedule, self.f_player)