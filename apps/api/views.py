# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.template import RequestContext
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django import get_version

from api.entry.models import Entry
from api.activity.utils import get_public_activities

@login_required
def logout_view(request):
    logout(request)

    return HttpResponseRedirect('/')

@login_required
def home_page_view(request):
    # Get Django current version
    django_version = get_version()

    #entries = Entry.objects.select_related().filter(is_visible=True).order_by('-created_date')
    entries = Entry.objects.select_related().order_by('-created_date')

    activities = get_public_activities(request.user)

    return render_to_response('home.html', {'nbar': 'home', 'entries': entries, 'activities': activities, 'django_version': django_version}, context_instance=RequestContext(request))