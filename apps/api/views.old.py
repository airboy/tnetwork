# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.template import RequestContext
from django.db.models import Q
from django.utils.simplejson import dumps
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import send_mail, BadHeaderError, EmailMultiAlternatives
from email.MIMEImage import MIMEImage
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from tnetwork.apps.api.models import *
from tnetwork.apps.discus.models import Discus
from tnetwork.apps.entry.models import Entry

from tnetwork.libs.discus_tools import (get_all_discus, list_discus_to_json)
from tnetwork.libs.daymatch_tools import (get_current_daymatch, get_daymatch_at_date, daymatch_progs_to_json)

from tnetwork.settings.common import BUZZY
import datetime
import hashlib, md5
import simplejson as json


def home(request):
    entries = Entry.objects.select_related().filter(is_visible=True).order_by('-created_date')
    return render_to_response('home.html', {'nbar': 'home', 'entries': entries, "sysdate": datetime.datetime.now()}, context_instance=RequestContext(request))

def cal_view(request):
    """
    Load the calendar (list of all Daymatch) within the HTML page and get DaymatchProg and Discus for current Daymatch.
    """
    # Init variables to None (to be able to display page with empty values)
    current = None
    daymatch_progs = None
    list_discus = None

    # Get a list of all Dyamatch instances
    daymatchs = Daymatch.objects.all()

    # Check if Daymatch instances are found
    if daymatchs != []:
        # Get an instance of current Daymatch
        current = get_current_daymatch()

        # Check if a Daymatch instance exists for the current date (datetime.date.today())
        if current != None:
            # Get a list of all DaymatchProgs for the current Daymatch
            daymatch_progs = DaymatchProg.objects.select_related().filter(f_daymatch=current)

            # Get a list of tuples which contains All Discus and DiscusSons for the current Daymatch
            list_discus = get_all_discus(current)

    return render_to_response('calendar.html', {'nbar': 'calendar', 'daymatchs': daymatchs, 'current': current, 'daymatch_progs': daymatch_progs, 'list_discus': list_discus}, context_instance=RequestContext(request))

def ymd_api_view(request, year, month, day):
    """
    Handle asynchronous request for a Daymatch.

    REQUIRES : The date formed with the formal parameters must be valid.
        Only HTTP GET request are handle.
    EFFECTS : Return a JSON string representation containing all DaymatchProg and Discus data for the targeted Daymatch.
    """
    if request.method == 'GET':
        # Check if a Daymatch instance for the date formed with formal parameters exists.
        # We assume the risk of error with the Datetime.date constructor if parameters are not corrects. 
        current = get_daymatch_at_date(datetime.date(int(year), int(month), int(day)))

        if current != None:
            # Get a list of all DaymatchProgs for the current Daymatch
            daymatch_progs = DaymatchProg.objects.select_related().filter(f_daymatch=current)

            # Get a list of All Discus for the current Daymatch
            list_discus = Discus.objects.filter(f_daymatch=current)

            return HttpResponse(json.dumps(u"{'daymatch_progs': " + daymatch_progs_to_json(daymatch_progs) + ", 'list_discus': " + list_discus_to_json(list_discus) +  "}"))
        else:
            return HttpResponse(u"{'error': 'No data found'}")
    else:
        # Return an HttpResponse object with a status code equals to 405 : Method not allowed
        return HttpResponse(json.dumps(u"{'error': '405 Method not allowed'}"), status=405)

