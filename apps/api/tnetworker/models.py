# -*- coding: utf-8 -*-

"""
These classes extend respectively BaseUserManager and AbstractBaseUser
abstract classes to define a custom User model compared to the Django
default User model. This capability is new from Django 1.5.
"""

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from tnetwork.libs.utils import generate_token
from api.daymatch.models import (Schedule, Playgroup)

class TNetworkerManager(BaseUserManager):
    def create_user(self, username, first_name, last_name, email, password):
        """
        Creates and saves a User with the given username, first_name, last_name, email and password. All these parameters are required.
        """
        if not username:
            raise ValueError('Users must have a username')
        if not first_name:
            raise ValueError('Users must have a first name')
        if not last_name:
            raise ValueError('Users must have a last name')
        if not email:
            raise ValueError('Users must have an email address')
        if not password:
            raise ValueError('Users must have a password')

        now = timezone.now()
        mail = TNetworkerManager.normalize_email(email)
        user = self.model(username=username, first_name=first_name, last_name=last_name, email=email, is_staff=False, is_active=True, is_superuser=False, last_login=now, date_joined=now, token=generate_token(username))
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, first_name, last_name, email, password):
        super_user = self.create_user(username, first_name, last_name, email, password)
        super_user.is_staff = True
        super_user.is_superuser = True
        super_user.save(using=self._db)
        return super_user

    def create_staff(self, username, first_name, last_name, email, password):
        staff = self.create_user(username, first_name, last_name, email, password)
        staff.is_staff = True
        staff.save(using=self._db)
        return staff


class TNetworker(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(verbose_name='email_address', max_length=255, unique=True, db_index=True)
    gsm = models.CharField(max_length=13, null=True, blank=True)
    date_joined = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    registration_date = models.DateTimeField(blank=True, null=True)
    token = models.CharField(max_length=64, blank=True, null=True)

    # Custom parameters
    f_playgroup = models.ForeignKey(Playgroup, blank=True, null=True)
    f_schedule = models.ForeignKey(Schedule, blank=True, null=True)
    internal_counter = models.SmallIntegerField(default=0)

    objects = TNetworkerManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        verbose_name = u'tnetworker'
        verbose_name_plural = u'tnetworkers'

    def __unicode__(self):
        return self.email

    def get_full_name(self):
        """
        Returns the first_name (capitalized) plus the last_name (Uppercased), with a space in between.
        """
        return "%s %s" % ((self.first_name).capitalize(), (self.last_name).upper())

    def get_middle_name(self):
        """
        Returns the first_name (capitalized) plus the last_name (Uppercased), with a space in between.
        """
        return "%s. %s" % ((self.first_name[:1]).upper(), (self.last_name).upper())

    def get_short_name(self):
        """Returns the short name for the user."""
        return self.first_name

    def is_admin(self):
        return self.is_staff

    def disable_account(self):
        """
        Set the is_active tasker attribute to False.
        """
        # Set user as inactive
        self.is_active = False
        self.save(update_fields=['is_active'])
        return True