# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from .views import home_page_view, logout_view
from api.entry.views import save_like_view

urlpatterns = patterns('',

	# Home page
    (r'^$', home_page_view),

    # Logout
    (r'^logout/$', logout_view),

    # Entry
    (r'^entry/like/(?P<entry_id>[0-9]+)/$', save_like_view),
)