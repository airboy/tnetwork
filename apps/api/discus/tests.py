"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from tnetwork.api.models import daymatch
from tnetwork.discus.models import *
import datetime

def test_discus():
	date = datetime.date(2012, 12, 27)
	dm = daymatch.objects.get(date=date)

	dis = discus(author='Romain FONCIER', mail='rfoncier@examle.com', body_mkd='First test for discus template', target=dm, validated=True, has_sons=True)
	dis.save_first()

	sons = discus_sons(author='Romain FONCIER', mail='rfoncier@example.com', body_mkd='First comment for the first post', parent=dis, validated=True)
	sons.save_first()