# -*- coding: utf-8 -*-

"""
This module contains Discus models which allows to manage conversation 
on the tnetwork platform.
"""

from django.db import models
from django.conf import settings
from django.utils import timezone
import markdown
import simplejson as json

# --- Discus ------------
class Discus(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    f_daymatch = models.ForeignKey('daymatch.Daymatch')
    created_date = models.DateTimeField(auto_now_add=True, default=timezone.now)
    modified_date = models.DateTimeField(auto_now=True)
    body = models.TextField(max_length=140, verbose_name='discus_body')
    has_sons = models.BooleanField(verbose_name=u'has_sons', default=False)

    class Meta:
        ordering = ('created_date',)
        get_latest_by = 'created_date'

    def __unicode__(self):
        return self.body

    def delete(self):
        if self.has_sons:
            # Deleting all sons for this discus
            DiscusSon.objects.filter(f_discus=self).delete()
        super(Discus, self).delete()

    def get_all_sons(self):
        """
        Return a list of validated DiscusSons for which self is a parent. If self has not sons, return None.
        """
        if self.has_sons:
            return DiscusSon.objects.filter(f_discus=self)
        else:
            None

    def get_json(self):
        """
        Return a JSON string representation of self containing only id, created_date, author, body fields and a list of sons if self has sons.
        """
        data = {}

        data['id'] = self.id
        data['created_date'] = self.created_date.strftime("%Y-%m-%d")
        data['author'] = self.author.get_middle_name()
        data['body'] = self.body

        # Check if self has sons and get DiscusSon data
        sons = []
        if self.has_sons:
            for ds in DiscusSon.objects.filter(f_discus=self):
                sons.append(ds.get_json())
        data['sons'] = sons

        # Return a JSON string representation of data
        return json.dumps(data)

# --- DiscusSon ------------
class DiscusSon(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    f_discus = models.ForeignKey(Discus)
    created_date = models.DateTimeField(auto_now_add=True, default=timezone.now)
    modified_date = models.DateTimeField(auto_now=True)
    body = models.TextField(blank=True, null=True, verbose_name='discus_sons_body')

    class Meta:
        ordering = ('created_date',)
        get_latest_by = 'created_date'

    def __unicode__(self):
        return self.body

    def get_json(self):
        """
        Return a JSON string representation of self containing only id, created_date, author and body fields.
        """
        data = {}

        data['id'] = self.id
        data['created_date'] = self.created_date.strftime("%Y-%m-%d")
        data['author'] = self.author.get_middle_name()
        data['body'] = self.body

        # Return a JSON string representation of data
        return json.dumps(data)
