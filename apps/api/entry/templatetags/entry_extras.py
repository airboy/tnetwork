# -*- coding: utf-8 -*-

from django import template

register = template.Library()

def check_liker(entry, liker):
	"""
	Return True if current user is a liker for this entry, 
	otherwise return False.
	"""
	return entry.is_a_liker(liker)

def get_likers(entry, cuser):
	"""
	Return all likers for this entry if they exist,
	except current user.
	"""
	return ", ".join(entry.get_likers(cuser))

def get_nb_likers(value):
	return value - 1

register.filter('check_liker', check_liker)
register.filter('get_likers', get_likers)
register.filter('get_nb_likers', get_nb_likers)