# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import ensure_csrf_cookie

import simplejson as json
from user_agents import parse

from api.activity.models import Activity
from api.activity.utils import get_html_activity
from .models import Entry

@login_required
@ensure_csrf_cookie
def save_like_view(request, entry_id):
	"""
	Process the like request. Save the like for user given in parameter if it's possible and return a JSON string response, otherwise return a JSON string error message.

	Return aHTML string within the response containing the notification body.
	"""
	if request.method == 'POST' and request.POST and request.is_ajax():
		# Keep trace about USER AGENT STRING
		ua = parse(request.META['HTTP_USER_AGENT'])
		browser = "%s/%s" % (ua.browser.family, ua.browser.version_string)
		system = "%s/%s" % (ua.os.family, ua.os.version_string)

		print "Hello"

		# Try to find the targeted entry
		try:
			entry = Entry.objects.get(pk=entry_id)

			if entry.save_like(request.user):
				# Create a new public activity
				a = Activity(f_user=request.user, f_object='ENTRY', f_task='LIKE', f_status='SUCCESS', target_id=entry.id, ip_address=request.META['REMOTE_ADDR'], browser=browser, system=system)
				a.save()

				return HttpResponse(content=json.dumps({"status": "success", "notification": get_html_activity(a, request.user), "likers": entry.get_likers(request.user)}), status=200)
			else:
				return HttpResponse(content=json.dumps({"status": "success", "msg": "User has already like the post : " + entry_id}), status=200)
		except Entry.DoesNotExist:
			# Create a new logging activity
			a = Activity(f_user=request.user, f_object='ENTRY', f_task='LIKE', f_status='ERROR', target_id=entry_id, ip_address=request.META['REMOTE_ADDR'], browser=browser, system=system, content='The targeted entry was not found')
			a.save()

			# Return a HTTP response with status 404 : Entry not found
			return HttpResponse(content=json.dumps({"status": "error", "msg": "The targeted entry was not found"}), status=404)

	else:
		# Handle only HTTP POST methods
		return HttpResponse(content=json.dumps({"status": "error", "msg": "The method used is not allowed"}), status=405)