from django.db import models
from django.conf import settings
from django.utils import timezone
import markdown

# --- Entry ------------
class Entry(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='author')
    created_date = models.DateTimeField(auto_now_add=True, default=timezone.now)
    modified_date = models.DateTimeField(auto_now=True)
    published_date = models.DateTimeField(blank=True, null=True)
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True, verbose_name='Message')
    body_md = models.TextField(blank=True, null=True, verbose_name='Message Markdown')
    is_visible = models.BooleanField(default=False)
    likers = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='likers')
    nb_likers = models.IntegerField(default=0)

    class Meta:
        ordering = ('-created_date',)
        get_latest_by = 'created_date'

    def __unicode__(self):
        return self.title

    def save(self):
        self.body = markdown.markdown(self.body_md, safe_mode='escape')
        super(Entry, self).save()

    def publish(self):
    	"""
    	Publish the current entry (set visible).
    	"""
        self.is_visible = True
        self.published_date = timezone.now
        super(Entry, self).save(update_fields=['is_visible', 'published_date'])

    def is_a_liker(self, user):
        """
        Check if current user has like this entry.
        """
        return user in self.likers.all()

    def get_likers(entry, cuser):
        """
        Return all likers for this entry if they exist,
        except current user as a Python list.
        """
        result = []
        for liker in entry.likers.all():
            if liker != cuser:
                result.append(liker.get_middle_name())
        return result

    def get_all_likers(entry):
        """
        Return all likers for this entry if they exist as a Python list.
        """
        return [liker.get_middle_name() for liker in entry.likers.all()]

    def save_like(self, liker):
        """
        Check if current liker has already hit a like. If not add it as a liker for this entry, increase nb_likers values by 1 and return True, otherwise return False
        """
        if self.is_a_liker(liker):
            return False
        else:
            self.likers.add(liker)
            self.nb_likers += 1
            super(Entry, self).save(update_fields=['nb_likers'])
        return True

    def unlike(self, liker):
        """
        Check if current liker has already hit a like.
        If true, remove current liker from likers list, dicrease nb_likers value and return True, otherwise return False.
        """
        if self.is_a_liker(liker):
            self.likers.remove(liker)
            self.nb_likers -= 1
            super(Entry, self).save(update_fields=['nb_likers'])
            return True
        else:
            return False
