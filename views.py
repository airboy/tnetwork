# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib import messages
from forms import LoginForm

from user_agents import parse

from api.activity.models import (ConnectionFailed, Activity)

@csrf_protect
def first_page_view(request):
    no_script = False

    if (request.method == 'GET' and 'no_script' in request.GET):

        if (request.GET['no_script'] == 'true'):
            no_script = True
    
    elif request.method == 'POST' and request.POST:
        form = LoginForm(request.POST)
        
        if form.is_valid():
            cd = form.cleaned_data

            # Validation
            username = cd['username']
            password = cd['password']
            
            user = authenticate(username=username, password=password)
        
            if user is not None:
                # Keep trace about USER AGENT STRING
                ua = parse(request.META['HTTP_USER_AGENT'])
                browser = "%s/%s" % (ua.browser.family, ua.browser.version_string)
                system = "%s/%s" % (ua.os.family, ua.os.version_string)

                if user.is_active:
                    login(request, user)

                    a = Activity(f_user=user, f_object='USER', f_task='LOGIN', f_status='SUCCESS', ip_address=request.META['REMOTE_ADDR'], browser=browser, system=system)
                    a.save()

                    return HttpResponseRedirect('/app/')

                else:
                    a = Activity(f_user=user, f_object='USER', f_task='LOGIN', f_status='ERROR', ip_address=request.META['REMOTE_ADDR'], browser=browser, system=system, content='User is not active')
                    a.save()

                    messages.error(request, 'Vous n\'avez pas accès à cette section !')

            else:
                # Keep trace of activity
                cf = ConnectionFailed(ip_address=request.META['REMOTE_ADDR'], username=request.POST['username'], password=request.POST['password'])
                cf.save()

                messages.warning(request, 'L\'utilisateur et/ou mot de passe saisis sont incorrects !')

        else:
            # Keep trace of activity
            cf = ConnectionFailed(ip_address=request.META['REMOTE_ADDR'], username=request.POST['username'], password=request.POST['password'])
            cf.save()

            messages.warning(request, 'Les données saisies sont incorrectes !')
    
    return render_to_response('firstpage.html', {'no_script': no_script}, context_instance=RequestContext(request))