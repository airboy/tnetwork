# -*- coding: utf-8 -*-

from tnetwork.api.models import *
import md5, uuid, hashlib
from xlrd import open_workbook,cellname
from datetime import date, timedelta


def create_user_uuid():
	""" Create a new UUID table for player """
	res = player.objects.all()
	for pl in res:
		(id_uuid, short_uuid) = pl.generate_new_uuid('')

		# Update table player
		pl.uuid_user = id_uuid
		pl.save()

		# Update table uuid_pl
		uu = uuid_pl(id_uuid=id_uuid, short_uuid=short_uuid)
		uu.save()

		print(u'New UUID generated for user :'+pl)

	return ('Work Done!')

def fill_from_xls():
	book = open_workbook('tennis.xls')
	sheet = book.sheet_by_index(0)

	for row_index in range(1, 31):
		w = []
		for col_index in range(1, 6):
			w.append(sheet.cell(row_index,col_index).value)
		ln = w[0].split(' ')
		if len(ln) > 2:
			name = ln[0].lower()+' '+ln[1].lower()
			fname = ln[2].lower()
		else:
			name = ln[0].lower()
			fname = ln[1].lower()

		tmp = w[3].replace('*', '')
		if tmp == '19h00':
			tp = 1
		elif tmp == '20h30':
			tp = 2

		if w[4] != '' and int(w[4]) == 1:
			ht = 1
		else:
			ht = 0

		#print(name+' '+fname+' '+w[1]+' '+w[2]+' '+str(ht)+' '+str(tp))
		pl = player(name=name, fname=fname, gsm=w[1], mail=w[2], half_time=ht, time_pref=tp)
		pl.save_first()

def fill_calendar(dstart, wstart, wstop):
	""" dstart is a datetime.date object, wstart and wstop are simple integer """
	c = dstart
	dm1 = daymatch(date=c)
	dm1.save_first()
	for w in range (wstart, wstop):
		c = c+timedelta(days=7)
		dm = daymatch(date=c)
		dm.save_first()

def add_players():
	p1 = player.objects.get(uuid_user='5b8aca835de85e32a355e6624ab5ff70')
	p2 = player.objects.get(uuid_user='0267ef782b665e1daf6c03073c78e500')
	for i in range(52, 53):
		d = daymatch.objects.get(week=i)
		d.time_first.add(p1)
		d.time_first.add(p2)
