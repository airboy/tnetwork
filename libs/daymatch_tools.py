# -*- coding: utf-8 -*-

from tnetwork.apps.api.models import (Schedule, Daymatch)
import datetime
import simplejson as json

"""
This class provides utility methods to manipulate Daymatch instances.
"""

def get_current_daymatch():
	"""
	Return an instance of the current Daymatch if it exists. If no datat found, return None.
	"""
	# Get current datetime (Date at the method execution)
	(c_year, c_week, c_day) = datetime.date.today().isocalendar()

	# Try to find the current Daymatch instance
	try:
		return Daymatch.objects.get(date__year=c_year, week=c_week)
	except Daymatch.DoesNotExist:
		return None

def get_daymatch_at_date(date):
	"""
	REQUIRES : date must be a datetime.date instance.
	EFFECTS : Return a Daymatch instance if it has been found for this date, otherwise return None.
	"""
	try:
		return Daymatch.objects.get(date=date)
	except Daymatch.DoesNotExist:
		return None

def daymatch_progs_to_json(daymatch_progs):
	"""
	REQUIRES : daymatch_progs must not be empty (List of DaymatchProg instances) and must containt related objects informations.
	EFFECTS : Return a JSON string representation of DaymatchProg data containing two array with players's names.

	{
		'19h00': ['player 1', ],
		'20h30': ['player 2', ],
		...
	}
	"""
	# Init Python dict with keys equals to all schedule values. Dict values are initialize to an empty list.
	data = {}
	for s in Schedule.objects.all():
		data[s.description] = []

	# Iterate through daymatch_progs
	for dmp in daymatch_progs:
		data[dmp.f_schedule.description].append(dmp.f_player.__unicode__())

	# Return a JSON string representation of data
	return json.dumps(data)
