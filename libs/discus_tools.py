# -*- coding: utf-8 -*-

from tnetwork.apps.discus.models import (Discus, DiscusSon)
import simplejson as json

"""
This class provides utility methods to manipulate Discus and DiscusSon instances.
"""

def get_all_discus(target_daymatch):
	"""
	Return a list of tuple containing all validated Discus and its sons for the targeted Daymatch.
	REQUIRES : target_daymatch must be an instance of Daymatch.

	Daymatch --> [(Discus, [DiscusSon,]),]
	"""
	# Initialize the return value to an empty list.
	data = []

	# Iterate over all validated Discus instances for the targeted_daymatch and get their sons.
	for d in Discus.objects.filter(f_daymatch=target_daymatch, validated=True):
		data.append((d, d.get_all_sons()))

	return data

def list_discus_to_json(list_discus):
	"""
	REQUIRES : list_discus must not be empty (List of Discus instances).
	EFFECTS : Return a JSON string representation of an array of Discus data and arrays with DiscusSon data.

	[
		{
			'id': 1,
			'created_date': 'dd-mm-yyyy',
			'author': 'Romain FONCIER',
			'body': 'My comments',
			'sons': [
				{
					'id': 2,
					'created_date': 'dd-mm-yyyy',
					'author': 'Another player',
					'body': 'Another comments',
				},
				...
			]
		},
		...
	]
	"""
	# Init Python list to an empty list.
	data = []
	# Iterate through list_discus
	for ld in list_discus:
		d_data = {}

		d_data['id'] = ld.id
		d_data['created_date'] = ld.created_date.strftime("%Y-%m-%d")
		d_data['author'] = ld.author.__unicode__()
		d_data['body'] = ld.body

		# Check if current Discus has sons and get DiscusSon data
		sons = []
		if ld.has_sons:
			for ds in DiscusSon.objects.filter(f_discus=ld):
				ds_data = {}

				ds_data['id'] = ds.id
				ds_data['created_date'] = ds.created_date.strftime("%Y-%m-%d")
				ds_data['author'] = ds.author.__unicode__()
				ds_data['body'] = ds.body

				sons.append(ds_data)
		d_data['sons'] = sons

		data.append(d_data)

	# Return a JSON string representation of data
	return json.dumps(data)
