# -*- coding: utf-8 -*-

"""
Utility functions used like tools to generate UUID or other specific values.
"""

import hashlib
import time

def generate_token(salt):
    """
    Generate new token based on SHA256 Hash Algorithm and current processor time, with a length of 40 characters.
    """
    # Check if salt is a instance of String
    if not isinstance(salt, str):
    	salt = str(salt)

    # Generate Hash string
    token = hashlib.sha256(str(time.clock())+salt).hexdigest()
    return token