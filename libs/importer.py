# -*- coding: utf-8 -*-

import anyjson
from datetime import date
from tnetwork.settings.common import PROJECT_ROOT
from tnetwork.apps.api.models import (Player, Daymatch, Schedule, Playgroup, DaymatchProg)

def import_old_data():
	# Import and convert players
	f = open(PROJECT_ROOT / "libs/to_import/api_player.json", 'r')
	players = anyjson.deserialize(f.read())
	f.close()

	# Import and convert daymatch
	f = open(PROJECT_ROOT / "libs/to_import/api_daymatch.json", 'r')
	daymatchs = anyjson.deserialize(f.read())
	f.close()

	# Import and convert time_first
	f = open(PROJECT_ROOT / "libs/to_import/api_daymatch_time_first.json", 'r')
	tfirst = anyjson.deserialize(f.read())
	f.close()

	# Import and convert time_second
	f = open(PROJECT_ROOT / "libs/to_import/api_daymatch_time_second.json", 'r')
	tsecond = anyjson.deserialize(f.read())
	f.close()

	# STEP 1 - Create new players
	pg = Playgroup.objects.get(pk=2)
	for player in players:
		p = Player(name=player['name'], fname=player['fname'], gsm=player['gsm'], email=player['mail'], f_playgroup=pg, f_schedule=Schedule.objects.get(pk=player['time_pref']))
		p.save()

	# STEP 2 - Create new daymatchs and daymatch_progs
	sch1 = Schedule.objects.get(pk=1)
	sch2 = Schedule.objects.get(pk=2)
	for dm in daymatchs:
		d = dm['date'].split('-')
		ndm = Daymatch(date=date(int(d[0]), int(d[1]), int(d[2])), week=dm['week'], validated=dm['validated'])
		ndm.save()

		# STEP 2.1 - Add new daymatch_progs from time_first
		for tf in tfirst:
			if tf['daymatch_id'] == dm['uuid_day']:
				# Get current player
				for pl in players:
					if pl['uuid_user'] == tf['player_id']:
						link_email = pl['mail']

				ndmp = DaymatchProg(f_daymatch=ndm, f_schedule=sch1, f_player=Player.objects.get(email=link_email))
				ndmp.save()

		# STEP 2.2 - Add new daymatch_progs from time_second
		for ts in tsecond:
			if ts['daymatch_id'] == dm['uuid_day']:
				# Get current player
				for pl in players:
					if pl['uuid_user'] == ts['player_id']:
						link_email = pl['mail']

				ndmp = DaymatchProg(f_daymatch=ndm, f_schedule=sch2, f_player=Player.objects.get(email=link_email))
				ndmp.save()



if __name__ == "__main__":
	import_old_data()
