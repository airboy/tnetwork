# -*- coding: utf-8 -*-

from django.utils.datastructures import SortedDict
from api.models import (Daymatch)
import datetime

class Calendar:
    """ This class defined a calendar object which represents the current state of the daymatch object in the database. These methods allows to update or reload the representation of this under the sorted dict form
    """

    def __init__(self):
        """
        This method contructs a calendar object under a dict representation
        """

        # Current start season
        starter = datetime.date(2013, 9, 1)

        # Calendar initialization
    	# Current daymatch
        (c_year, c_week, c_day) = datetime.date.today().isocalendar()
        dayms = Daymatch.objects.filter(date__gte=starter).order_by('date')
        self.calendar = SortedDict()
        # Initialize this variable to the first occurence of daymatch by default
        self.current = dayms[0].date


        # Treatment
        for dm in dayms:
            # Get date on current daymatch
            d = dm.date

            # Get the actual daymatch
            if (d.year == c_year and dm.week == c_week):
                self.current = d

            if (d.year in self.calendar):
                if (d.month in self.calendar[d.year]):
                    self.calendar[d.year][d.month].append(d.day)
                else:
                    self.calendar[d.year][d.month] = [d.day]
            else:
                self.calendar[d.year] = SortedDict([(d.month, [d.day])])

    def get_current_date(self):
        return self.current

    def get_current_date_format(self):
        (y, m, d) = (str(self.current.year), str(self.current.month), str(self.current.day))
        return y + '/' + m + '/' + d

    def check_update(self):
        """
        Return True if it's necessary to update the actual date.
        """
        (c_year, c_week, c_day) = datetime.date.today().isocalendar()
        d = None
        try:
            d = Daymatch.objects.get(date__year=c_year, week=c_week).date
            self.current = d
            return True
        except Daymatch.DoesNotExist:
            return False
            