### CSRF methods ###
csrfSafeMethod = (method) ->
	# these HTTP methods do not require CSRF protection
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))

sameOrigin = (url) ->
    # test that a given url is a same-origin URL
    # url could be relative or scheme relative or absolute
    host = document.location.host # host + port
    protocol = document.location.protocol
    sr_origin = '//' + host
    origin = protocol + sr_origin
    # Allow absolute or scheme relative URLs to same origin
    (url === origin || 
    url.slice(0, origin.length + 1) === origin + '/') ||
    (url === sr_origin || url.slice(0, sr_origin.length + 1) === sr_origin + '/') ||
    !(/^(\/\/|http:|https:).*/.test(url))

$.ajaxSetup(
    beforeSend: (xhr, settings) ->
        if !csrfSafeMethod(settings.type) && sameOrigin(settings.url)
            # Send the token to same-origin, relative URLs only.
            # Send the token only if the method warrants CSRF protection
            # Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken)