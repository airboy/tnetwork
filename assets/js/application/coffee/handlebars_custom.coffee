# Handlebars Custom helpers
Handlebars.registerHelper "if_gt", (v1, v2, options) ->
	if v1 > v2 then options.fn(this) else options.inverse(this)

Handlebars.registerHelper "join", (items, separator) ->
	r = new RegExp("^[,\.;: ]+$")
	if r.test(separator)
		items.join(separator)
	items.join(', ')

Handlebars.registerHelper "safe", (content) ->
	Handlebars.Utils.escapeExpression(content)