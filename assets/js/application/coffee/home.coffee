### CSRF methods ###
csrfSafeMethod = (method) ->
    # these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))

sameOrigin = (url) ->
    # test that a given url is a same-origin URL
    # url could be relative or scheme relative or absolute
    host = document.location.host # host + port
    protocol = document.location.protocol
    sr_origin = '//' + host
    origin = protocol + sr_origin
    # Allow absolute or scheme relative URLs to same origin
    (url == origin || 
    url.slice(0, origin.length + 1) == origin + '/') ||
    (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
    !(/^(\/\/|http:|https:).*/.test(url))

$.ajaxSetup
    beforeSend: (xhr, settings) ->
        if !csrfSafeMethod(settings.type) && sameOrigin(settings.url)
            # Send the token to same-origin, relative URLs only.
            # Send the token only if the method warrants CSRF protection
            # Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'))

### Template manipulations ###
get_rendered_template = (template_name, data) ->
    template = Handlebars.compile($(template_name).html())
    template(data)

### Main execution ###
jQuery ->
	# Handle tooltip
	$('.entry-likers-tooltip').tooltip(
		placement: top
	)

	# Handle like actions
	$('#ilike').on "click", (event) ->
		# Get start time
        stime = new Date()

        # Get entry id
		target_id = $(this).data('entry-id')

        # Get parent container
        parent = $(this).parent()

		# Send POST request
		$.post '/app/entry/like/' + target_id + '/',
			'csrfmiddlewaretoken': $.cookie('csrftoken'),
            (data) ->
                data = JSON.parse data
                switch data.status
                    when 'success'
                        # Add content to data JSON object
                        data.target_id = target_id
                        data.separator = ", "
                        data.notification_timing = '1 sec'

                        # Update HTML content
                        $(this).prev().remove()
                        $(this).remove
                        parent.append(get_rendered_template('#tpl-entry-likers', data))

                        # Prepend new notification to activity feeds
                        $('#activity-container').prepend(get_rendered_template('#tpl-activity-item'), data) 
                true