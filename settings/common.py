# Common settings for TNetwork project.

import os
import sys
import warnings; warnings.simplefilter("ignore")
from path import path
from django.conf.global_settings import *

ADMINS = (
    ('Romain', 'romain@example.com'),
)

MANAGERS = ADMINS

#==================================================================
# Generic Django project settings
#==================================================================

# Password for access to the platform
SITE_ID = 1
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'UTC'
USE_TZ = True
USE_I18N = False
USE_L10N = False
LANGUAGE_CODE = 'fr'
LANGUAGES = (
    ('fr', 'French'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = os.environ['SECRET_KEY']

INSTALLED_APPS = (
    # tnetwork apps
    'tnetwork.apps.api.tnetworker',
    'tnetwork.apps.api.daymatch',
    'tnetwork.apps.api.entry',
    'tnetwork.apps.api.activity',
    'tnetwork.apps.api.discus',

    # Application dependencies
    'south',
    'pipeline',
    'templatetag_handlebars',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.admin',
    # 'django.contrib.admindocs',
)

#==================================================================
# Project URLS and media settings
#==================================================================

ROOT_URLCONF = 'tnetwork.urls'

PROJECT_ROOT = path(__file__).abspath().dirname().dirname()
SITE_ROOT = PROJECT_ROOT.dirname()
STATIC_DIR = PROJECT_ROOT / 'assets'

STATIC_ROOT = '/media/'
MEDIA_ROOT = SITE_ROOT / 'uploads'

sys.path.append(SITE_ROOT)
sys.path.append(PROJECT_ROOT / 'apps')
sys.path.append(PROJECT_ROOT / 'libs')

STATIC_URL = '/static/'
MEDIA_URL = '/uploads/'

#==================================================================
# Assets settings
#==================================================================

STATICFILES_DIRS = (
    STATIC_DIR,
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'pipeline.finders.PipelineFinder',
    'pipeline.finders.CachedFileFinder',
)

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'

PIPELINE_COMPILERS = (
  'pipeline.compilers.coffee.CoffeeScriptCompiler',
  'pipeline.compilers.less.LessCompiler',
)

PIPELINE_CSS = {
    'main': {
        'source_filenames': (
          'css/less/base.less',
        ),
        'output_filename': 'css/main.css',
        'extra_context': {
            'media': 'screen,projection',
        },
    },
}

PIPELINE_JS = {
    'global': {
        'source_filenames': (
          'js/global/jquery-1.11.js',
          'js/plugins/jquery/jquery.cookie.js',
        ),
        'output_filename': 'js/global.js',
    },
    'bs3': {
        'source_filenames': (
          'js/bootstrap/affix.js',
          'js/bootstrap/button.js',
          'js/bootstrap/collapse.js',
          'js/bootstrap/dropdown.js',
          'js/bootstrap/tooltip.js',
        ),
        'output_filename': 'js/bs3.js',
    },
    'main': {
        'source_filenames': (
          'js/application/coffee/handlebars_custom.coffee',
          'js/application/coffee/home.coffee',
        ),
        'output_filename': 'js/main.js',
    }
}

#==================================================================
# Templates
#==================================================================

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    PROJECT_ROOT / 'templates',
)

sys.path.append(PROJECT_ROOT / 'templates')

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

#==================================================================
# Middleware
#==================================================================

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#==================================================================
# Auth / security
#==================================================================

LOGIN_URL = '/'
LOGOUT_URL = '/app/logout/'

AUTH_USER_MODEL = 'tnetworker.TNetworker'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)


#==================================================================
# Miscellaneous project settings
#==================================================================

import logging

LOG_DATE_FORMAT = '%d %b %Y %H:%M:%S'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': u'%(asctime)s | %(levelname)-7s | %(name)s | %(message)s',
            'datefmt': LOG_DATE_FORMAT,
        },
        'simple': {
            'format': '%(levelname)-7s | %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.db.backends': {
            'level': 'DEBUG',
            'handlers': ['console']
        },
        'subdomains.middleware': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
    }
}

#==================================================================
# Third party app settings
#==================================================================