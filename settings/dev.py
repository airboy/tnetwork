# Development settings fro tnetwork project.
from common import *
import dj_database_url

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES['default'] =  dj_database_url.config()

#INSTALLED_APPS += (
#	'debug_toolbar',
#)

ALLOWED_HOSTS = []

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')