# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from .views import first_page_view

urlpatterns = patterns('',

    # First page (Login)
    (r'^$', first_page_view),

    # App (API)
    (r'^app/', include('tnetwork.apps.api.urls')),
)